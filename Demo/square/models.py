from django.db import models
import numpy as np
# Create your models here.

def square(digit):
    #digit = int(input("Enter an integer number: "))

    #   calculate square using exponent operator
    square = digit**2

    # print
    print(f"Square of {digit} is {square}")
    return square

def square2():
    numbers = [11, 21, 19, 30, 46]

    squaredValues = [number ** 2 for number in numbers]

    # print
    print('Original Values: ', numbers)
    print('Squared Values: ', squaredValues)
    return numbers, squaredValues

def square3():
    arr = np.array([11, 19, 21, 29, 46])
    print("Square Value of arr1 : \n", np.square(arr))
    print("\n")
    arr2 = [-19, -21]
    print("Square Value of arr2 : \n", np.square(arr2))
    return

def div(a, b):
    if b != 0:
        c = a/b
    else:
        c = "you cant divide to zero"
    return c

def multi(a,b):
   return a*b

def sum(a,b):
    return a+b

def subtract(a,b):
    return a-b

def boolean(a):
    if a<0:
        return True
    else:
        return False
