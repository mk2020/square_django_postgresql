from django.test import TestCase
from .models import square,div,subtract, multi,sum
import random

from random import randrange

# Create your tests here.
class BasicTest(TestCase):
    def test_multi(self):
        x = random.randint(-50, 50)
        print(x)
        f = square(x)
        c = x**2
        self.assertEqual(f, c)

    def test_divide(self):
        x = random.randint(-50, 50)
        y = random.randint(-50, 50)
        if y == 0:
            c = div(x, y)
            f = "you cant divide to zero"
            self.assertEqual(f, c)
        else:
            c = div(x, y)
            f = x / y
            self.assertEqual(f, c)

    def test_sum(self):
        x = random.randint(-50, 50)
        y = random.randint(-50, 50)
        c = x+y
        f= sum(x,y)
        self.assertEqual(f, c)

    def test_sub(self):
        x = random.randint(-50, 50)
        y = random.randint(-50, 50)
        c = x - y
        f = subtract(x, y)
        self.assertEqual(f, c)


